import axios from "axios";
import { useState } from "react";
import { useHistory } from "react-router";
import WeddingTable from "./wedding-components/wedding-table";

export default function Planner()
{
    const [weddings, setWeddings] = useState([]);
    const [times, setTimes] = useState(0);
    const history = useHistory<any>();

    if (history.location.state)
    {
        if (history.location.state["deleted"])
        {
            //alert(`wedding with id ${history.location.state["id"]} was deleted.`);
            delete history.location.state["deleted"];
        }
    }

    async function getWeddings()
    {
        const response = await axios.get('http://localhost:3001/weddings');
        setWeddings(response.data);
    }

    if (times === 0)
    {
        getWeddings().then();
        setTimes(1);
    }

    return(
        <>
            <h1>Wedding Planner</h1>
            <h3>Welcome, {history.location.state["email"]}</h3>
            <button onClick = {()=>history.push("/login")}>Logout</button>
            <button onClick = {()=>history.push({pathname: "/chat", state: history.location.state})}>Chat</button>
            <button className = "btn btn-outline-primary" onClick = {()=>history.push({pathname: "/create-wedding", state: history.location.state})}>Create new wedding</button>
            <WeddingTable weddings={weddings}></WeddingTable>
        </>
    );
}