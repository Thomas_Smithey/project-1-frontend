import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router";

export default function EditWedding()
{
    const history = useHistory<any>();
    const dateInput = useRef<any>(null);
    const locationInput = useRef<any>(null);
    const nameInput = useRef<any>(null);
    const budgetInput = useRef<any>(null);

    return(
        <>
            <h3>Welcome, {history.location.state["email"]}</h3>
            <button onClick = {()=>history.push("/login")}>Logout</button>
            <label htmlFor = "date">Date: </label>
            <input name = "date" type = "date" ref = {dateInput} defaultValue = {history.location.state["w_date"]}></input><br/>

            <label htmlFor = "loc">Location: </label>
            <input name = "loc" ref = {locationInput} defaultValue = {history.location.state["w_location"]}></input><br/>

            <label htmlFor = "name">Name: </label>
            <input name = "name" ref = {nameInput} defaultValue = {history.location.state["w_name"]}></input><br/>

            <label htmlFor = "budget">Budget: </label>
            <input name = "budget" type = "number" ref = {budgetInput} defaultValue = {history.location.state["budget"]}></input><br/>

            <button onClick={
                async () =>
                {
                    const wedding:any =
                    {
                        id: history.location.state["id"],
                        w_date: dateInput.current.value,
                        w_location: locationInput.current.value,
                        w_name: nameInput.current.value,
                        budget: budgetInput.current.value
                    };
                    const response = await axios.put("http://localhost:3001/weddings", wedding);
                    wedding["email"] = history.location.state["email"];
                    history.push({pathname: "/view-wedding", state: wedding});
                }
                }>Submit</button>
            <button onClick = {()=>history.push({pathname: "view-wedding", state: history.location.state})}>Cancel</button>
        </>
    );
}