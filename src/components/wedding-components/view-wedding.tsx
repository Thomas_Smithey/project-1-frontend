import axios from "axios";
import { SyntheticEvent, useState } from "react";
import { useHistory } from "react-router";
import ExpenseTable from "../expense-components/expense-table";

export default function ViewWedding()
{
    const history = useHistory<any>();
    const [expenses, setExpenses] = useState([]);
    const [totalExpenses, setTotalExpenses] = useState(0);
    const [times, setTimes] = useState(0);

    const id = history.location.state["id"];
    const date = history.location.state["w_date"];
    const name = history.location.state["w_name"];
    const location = history.location.state["w_location"];
    const budget = history.location.state["budget"];

    async function edit(event:SyntheticEvent)
    {
        history.push({pathname:"/edit-wedding", state: history.location.state});
    }

    async function del(event:SyntheticEvent) // del = delete
    {
        const response = await axios.delete(`http://localhost:3001/weddings/${id}`);
        history.location.state["deleted"] = true;
        history.push({pathname:"/planner", state: history.location.state});
    }

    async function getExpenses()
    {
        const response = await axios.get(`http://localhost:3001/weddings/${id}/expenses`);
        setExpenses(response.data);
        let newTotal:number = 0;
        for (const i of response.data)
        {
            newTotal += Number(i["amount"]);
            setTotalExpenses(newTotal);
        }
    }

    if (times === 0)
    {
        getExpenses().then();
        setTimes(1);
    }

    return(
        <>
            <h3>Welcome, {history.location.state["email"]}</h3>
            <button onClick = {()=>history.push("/login")}>Logout</button>
            <p>id: {id}</p>
            <p>date: {date}</p>
            <p>name: {name}</p>
            <p>location: {location}</p>
            <p>budget: {budget}</p>

            <button onClick = {edit}>Edit</button>
            <button onClick = {del}>Delete</button>
            <button onClick = {()=>history.push({pathname: "/create-expense", state: history.location.state})}>Add expense</button>

            <ExpenseTable expenses = {expenses}></ExpenseTable>
            <p>total expenses: {totalExpenses}</p>
            <button onClick = {()=>history.push({pathname: "/planner", state: {email: history.location.state["email"]}})}>Return to planner</button>
        </>
    );
}