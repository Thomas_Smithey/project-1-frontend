import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router";

export default function NewWeddingForm()
{
    const history = useHistory<any>();
    const dateInput = useRef<any>(null);
    const locationInput = useRef<any>(null);
    const nameInput = useRef<any>(null);
    const budgetInput = useRef<any>(null);

    return(
        <>
            <h3>Welcome, {history.location.state["email"]}</h3>
            <button onClick = {()=>history.push("/login")}>Logout</button>
            <label htmlFor = "date">Date: </label>
            <input name = "date" type = "date" ref = {dateInput}></input><br/>

            <label htmlFor = "loc">Location: </label>
            <input name = "loc" ref = {locationInput}></input><br/>

            <label htmlFor = "name">Name: </label>
            <input name = "name" ref = {nameInput}></input><br/>

            <label htmlFor = "budget">Budget: </label>
            <input name = "budget" type = "number" ref = {budgetInput}></input><br/>

            <button onClick={
                async () =>
                {
                    const wedding =
                    {
                        w_date: dateInput.current.value,
                        w_location: locationInput.current.value,
                        w_name: nameInput.current.value,
                        budget: budgetInput.current.value
                    };
                    const response = await axios.post("http://localhost:3001/weddings", wedding);
                    history.push({pathname: "/planner", state: history.location.state});
                }
                }>Submit</button>
            <button onClick = {()=>history.push({pathname: "/planner", state: history.location.state})}>Cancel</button>
        </>
    );
}