import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router-dom";

export default function Login()
{
    const history = useHistory<any>();
    const emailInput = useRef<any>(null);
    const passwordInput = useRef<any>(null);
    return(
        <>
            <h1>Login</h1>
            <label htmlFor = "email">Email: </label>
            <input name = "email" ref = {emailInput} defaultValue = "tom@wed.com"></input><br/>

            <label htmlFor = "password">Password: </label>
            <input name = "amount" type = "password" ref = {passwordInput} defaultValue = "password"></input><br/>

            <button onClick={
                async () =>
                {
                    const employee =
                    {
                        email: emailInput.current.value,
                        password: passwordInput.current.value
                    };

                    try
                    {
                        const response = await axios.patch("https://authorization-service-dot-project-1-smithey.ue.r.appspot.com/users/login", employee);
                        history.push({pathname: "/planner", state: {email: employee.email}});
                    }
                    catch(error)
                    {
                        alert("Invalid login credentials.");
                    }
                }
                }>Login</button>
        </>
    );
}