import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router-dom";

export default function EditExpense()
{
    const history = useHistory<any>();
    const id = history.location.state["id"];
    const w_id = history.location.state["w_id"];
    const reasonInput = useRef<any>(null);
    const amountInput = useRef<any>(null);
    const photoInput = useRef<any>(null);

    return(
        <>
            <h3>Welcome, {history.location.state["email"]}</h3>

            <button onClick = {()=>history.push("/login")}>Logout</button>

            <label htmlFor = "reason">Reason: </label>
            <input name = "reason" ref = {reasonInput} defaultValue = {history.location.state["reason"]}></input><br/>

            <label htmlFor = "amount">Amount: </label>
            <input name = "amount" type = "number" ref = {amountInput} defaultValue = {history.location.state["amount"]}></input><br/>

            <label htmlFor = "photo">Photo: </label>
            <input type = "file" name = "photo" accept="image/png, image/jpeg" ref = {photoInput}></input><br/>

            <button onClick={
                async () =>
                {
                    // use btoa to get the chosen file encoded into base64.
                    const b64 = btoa(photoInput.current.value); // content

                    const value:number = Math.round(Math.random() * (300 - 0) + 0);

                    const image =
                    {
                        content: b64,
                        name: "image-file" + String(value),
                        extension: "jpg"
                    }

                    // send the encoded file to the cloud function url.
                    //https://us-central1-project-1-smithey.cloudfunctions.net/file-upload
                    const response = await axios.post("https://us-central1-project-1-smithey.cloudfunctions.net/file-upload", image);
                    
                    // store the returned value in the expense object.
                    const expense =
                    {
                        id: history.location.state["id"],
                        w_id: history.location.state["w_id"],
                        reason: reasonInput.current.value,
                        amount: amountInput.current.value,
                        photo: response.data["photoLink"]
                    };
                    await axios.put(`http://localhost:3001/expenses/${id}`, expense);
                    
                    const wedding = await axios.get(`http://localhost:3001/weddings/${w_id}`);
                    wedding.data["email"] = history.location.state["email"];
                    history.push({pathname: "/view-wedding", state: wedding.data});
                }
                }>Submit</button>
            <button onClick = {
            async () => 
            {
                const wedding = await axios.get(`http://localhost:3001/weddings/${w_id}`);
                wedding.data["email"] = history.location.state["email"];
                history.push({pathname: "/view-wedding", state: wedding.data});
            }}>Cancel</button>
        </>
    );
}