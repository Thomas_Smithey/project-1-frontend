import axios from "axios";
import { useRef } from "react";
import { useHistory } from "react-router";

export default function NewExpenseForm()
{
    const history = useHistory<any>();
    const reasonInput = useRef<any>(null);
    const amountInput = useRef<any>(null);
    const photoInput = useRef<any>(null);
    
    return(
        <>
            <h3>Welcome, {history.location.state["email"]}</h3>

            <label htmlFor = "reason">Reason: </label>
            <input name = "reason" ref = {reasonInput}></input><br/>

            <label htmlFor = "amount">Amount: </label>
            <input name = "amount" type = "number" ref = {amountInput}></input><br/>

            <label htmlFor = "photo">Photo: </label>
            <input type = "file" name = "photo" accept="image/png, image/jpeg" ref = {photoInput}></input><br/>

            <button onClick={
                async () =>
                {
                    // use btoa to get the chosen file encoded into base64.
                    const b64 = btoa(photoInput.current.value); // content

                    const value:number = Math.round(Math.random() * (300 - 0) + 0);

                    const image =
                    {
                        content: b64,
                        name: "image-file" + String(value),
                        extension: "jpg"
                    }

                    // send the encoded file to the cloud function url.
                    //https://us-central1-project-1-smithey.cloudfunctions.net/file-upload
                    const response = await axios.post("https://us-central1-project-1-smithey.cloudfunctions.net/file-upload", image);
                    
                    const expense =
                    {
                        w_id: history.location.state["id"],
                        reason: reasonInput.current.value,
                        amount: amountInput.current.value,
                        photo: response.data["photoLink"]
                    };
                    await axios.post("http://localhost:3001/expenses", expense);
                    history.push({pathname: "/view-wedding", state: history.location.state});
                }
                }>Submit</button>
            <button onClick = {()=>history.push({pathname: "/view-wedding", state: history.location.state})}>Cancel</button>
        </>
    );
}